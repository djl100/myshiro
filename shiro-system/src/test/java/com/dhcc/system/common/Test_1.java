package com.dhcc.system.common;

import org.apache.shiro.session.Session;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.exception.SerializationException;
import org.crazycake.shiro.serializer.ObjectSerializer;
import org.crazycake.shiro.serializer.RedisSerializer;
import org.crazycake.shiro.serializer.StringSerializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_1{
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Test
    public void set(){
        //redisTemplate.opsForValue().set("myKey","myValue");
        //System.out.println(redisTemplate.opsForValue().get("myKey"));

        RedisManager redisManager = new RedisManager();
        redisManager.setHost("127.0.0.1");
        redisManager.setPort(6379);
        redisManager.setPassword("foobared");
        try {
            RedisSerializer valueSerializer = new ObjectSerializer();
            RedisSerializer keySerializer = new StringSerializer();
            String key = "shiro:session:bcfde9f1-663b-4d2d-9b30-fa08347bbec3";
            System.out.println(keySerializer.serialize(key));
            byte[]  bytes = redisManager.get(keySerializer.serialize(key));
            System.out.println("-----------");
            ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
/*            ObjectInputStream objectInputStream = null;
            objectInputStream = new ObjectInputStream(byteStream);
            Object result = objectInputStream.readObject();*/
            Object result = valueSerializer.deserialize(bytes);
            Session s = (Session)result;
            System.out.println(s);
            Collection cons = s.getAttributeKeys();
            for(Object con:cons){
                System.out.println(con);
            }
            System.out.println(s.getAttribute("org.apache.shiro.subject.support.DefaultSubjectContext_PRINCIPALS_SESSION_KEY"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
