package com.dhcc.system.controller;

import com.dhcc.shiro.entity.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

    @ResponseBody
    @GetMapping("/login")
    public String login(User user) {
        if (StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getPassword())) {
            return "请输入用户名和密码！";
        }
        //用户认证信息
        Subject subject = SecurityUtils.getSubject();
        //封装用户名密码数据
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(
                user.getUserName(),
                user.getPassword()
        );
        try {
            //进行验证，这里可以捕获异常，然后返回对应信息
            subject.login(usernamePasswordToken);
            System.out.println("登录成功");
            String id = (String) subject.getSession().getId();
            System.out.println("id================= "+id);
            return "login success   "+id;
        } catch (UnknownAccountException e) {
            System.out.println("用户名不存在");
            return "用户名不存在！";
        } catch (AuthenticationException e) {
            System.out.println("账号或密码错误！");
            return "账号或密码错误！";
        } catch (AuthorizationException e) {
            System.out.println("没有权限");
            return "没有权限";
        }
    }

    @ResponseBody
    @RequiresPermissions("add")
    @GetMapping("/add")
    public String userAdd(){
        return "新增成功";
    }

    @ResponseBody
    @RequiresPermissions("query")
    @GetMapping("/query")
    public String userQuery(){
        return "查询成功";
    }





}
