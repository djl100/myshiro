package com.dhcc.shiro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Permissions implements Serializable {
    private String id;
    private String permissionsName;
}