package com.dhcc.organ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Hello world!
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.dhcc.organ","com.dhcc.shiro"})
public class OrganApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(OrganApplication.class,args);
    }
}
