package com.dhcc.organ.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    @ResponseBody
    @RequiresPermissions("add")
    @RequestMapping("/add")
    public String userAdd(){
        return "新增成功";
    }

    @ResponseBody
    @RequiresPermissions("query")
    @GetMapping("/query")
    public String userQuery(){
        return "查询成功";
    }





}
